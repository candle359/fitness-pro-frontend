'use strict';
// init
var gulp = require('gulp'),
    prefixer = require('gulp-autoprefixer'),
    pug = require('gulp-pug'),
    rigger = require('gulp-rigger'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    cssmin = require('gulp-minify-css'),
    plumber = require('gulp-plumber'),
    svgSprite = require('gulp-svg-sprite'),
    svgmin = require('gulp-svgmin'),
    cheerio = require('gulp-cheerio'),
    replace = require('gulp-replace'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    sourcemaps = require('gulp-sourcemaps'),
    cache = require('gulp-cache'),
    notify = require('gulp-notify'),
    del = require('del'),
    browserSync = require("browser-sync")
var path = {
    build: {
        buid: 'build/**/*',
        html: 'build/',
        js: 'build/js/',
        styles: 'build/css/',
        images: 'build/images/',
        fonts: 'build/fonts/'
    },
    src: {
        html: 'src/templates/*.{pug,html}',
        js: 'src/js/*.js',
        jsMainFile: 'src/js/custom/*.js',
        styles: 'src/scss/*.*',
        stylesPartials: 'src/scss/partials/',
        images: 'src/images/**/*.{png,jpg,jpeg}',
        sprite: 'src/sprite/*.*',
        spriteSvg: 'src/sprite-svg/*.*',
        spriteSvgTemplate: 'src/sprite-svg-template.scss',
        fonts: 'src/fonts/**/*.*',
    },
    watch: {
        html: 'src/**/*.{pug,html}',
        js: 'src/js/**/*.js',
        styles: 'src/scss/**/*.scss',
        images: 'src/images/**/*.*',
        spriteSvg: 'src/sprite-svg/*.*',
        sprite: 'src/sprite/*.*',
        fonts: 'src/fonts/**/*.*'
    }
};

gulp.task('clean', () => {
    return del([path.build.buid]);
});
gulp.task('browser-sync', done => {
    browserSync.init({
        notify: false,
        tunnel: false,
        server: {
            baseDir: "./build"
        }
    });
    done();
});
gulp.task('html:build', () => {
    return gulp.src(path.src.html)
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(pug({
            pretty: true,
        }))
        .pipe(gulp.dest(path.build.html))
        .pipe(plumber.stop());
});

gulp.task('js:build', done => {
    gulp.src(path.src.js)
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(rigger())
        .pipe(uglify())
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.js))
    gulp.src(path.src.jsMainFile)
        .pipe(gulp.dest(path.build.js));
    done();
});
gulp.task('js:buildDev', done => {
    gulp.src(path.src.js)
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.js))
    gulp.src(path.src.jsMainFile)
        .pipe(gulp.dest(path.build.js));
    done();
});

gulp.task('spriteSvg:build', () => {
    return gulp.src(path.src.spriteSvg)
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[stroke]').removeAttr('stroke');
                $('[style]').removeAttr('style');
            },
            parserOptions: {xmlMode: true}
        }))
        .pipe(replace('&gt;', '>'))
        .pipe(svgSprite({
            mode: {
                symbol: {
                    sprite: "../" + path.build.images + "sprite-svg.svg",
                    render: {
                        scss: {
                            "dest": '../' + path.src.stylesPartials + 'sprite-svg.scss',
                            "template": path.src.spriteSvgTemplate
                        }
                    },
                    example: true
                }
            }
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest("./"));
});

gulp.task('image:build', done => {
    return gulp.src(path.src.images)
        .pipe(cache(
            imagemin([
                imagemin.gifsicle({ interlaced: true }),
                imagemin.jpegtran({ progressive: true }),
                imagemin.optipng({ optimizationLevel: 5 }),
                imagemin.svgo({
                    plugins: [
                        {
                            removeViewBox: false,
                            collapseGroups: true
                        }
                    ]
                })
            ])
        ))
        .pipe(gulp.dest(path.build.images));
});

gulp.task('fonts:build', ()=> {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('styles:build', done => {
    return gulp.src(path.src.styles)
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(sass())
        .pipe(prefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
        .pipe(cssmin())
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.styles))

});

gulp.task('styles:buildDev', done => {
    gulp.src(path.src.styles)
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
        .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.styles))
    done();

});


gulp.task('build', gulp.series(
    'clean',
    gulp.parallel(
        'html:build',
        'js:build',
        'image:build',
        'spriteSvg:build',
        'styles:build',
        'fonts:build'
    )
));

function reload(done) {
    browserSync.reload();
    done();
};
var pathFileWach = path.src.html;
gulp.task('watchHtml:build', () => {
    return gulp.src(pathFileWach)
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest(path.build.html))
        .pipe(plumber.stop());
});

gulp.task('watchFiles', (done) => {
    gulp.watch(path.src.html, gulp.series('watchHtml:build', reload)).on('change', function(pathFile, stats) {
        pathFileWach = pathFile.replace(/\\/g,'/');
    });
    gulp.watch(['src/**/*.{pug,html}','!src/templates/*.{pug,html}'], gulp.series('watchHtml:build', reload));
    gulp.watch(path.watch.js, gulp.series('js:buildDev', reload));
    gulp.watch(path.watch.images, gulp.series('image:build', reload));
    gulp.watch(path.watch.spriteSvg, gulp.series('spriteSvg:build', reload));
    gulp.watch(path.watch.styles, gulp.series('styles:buildDev', reload));
    gulp.watch(path.watch.fonts, gulp.series('fonts:build', reload));
    done();
});

gulp.task('default', gulp.series('build'));
gulp.task('dev', gulp.series('build', 'browser-sync', 'watchFiles'));
