$(document).ready(function () {
  $('.social-box li a, .social-box li button, .social-lnks, .main-menu .social-menu a').click(function () {
    if ($(this).closest('.page-banner').length) {
      ym(25464830,'reachGoal','DGsocseti')
    } else {
      ym(25464830,'reachGoal','socseti');
    }
  });
  $('.blog-detail .banner a').click(function () {
    ym(25464830,'reachGoal','banner')
  });

  $('#callback_form').submit(function(e){
    ym(25464830,'reachGoal','ordercall')
  });
  $('#subscribe_form').submit(function(e){
    ym(25464830,'reachGoal','NEWSsubscription')
  });
  $('#subscribe_form_dg').submit(function(e){
    ym(25464830,'reachGoal','DGsubscription')
  });
  $('#subscribe_form-sect').submit(function(e){
    ym(25464830,'reachGoal','NEWSsubscription')
  });

  $('.land_form').submit(function(e){
    if ($(this).closest('.land-section-skills').length) {
      ym(25464830,'reachGoal','gift');
    } else if ($(this).hasClass('consult-form')) {
      $(this).removeClass('consult-form');
      ym(25464830,'reachGoal','konsultaciya');
    } else {
      ym(25464830,'reachGoal','sign-upp');
    }
  });
  $('#faq_form').submit(function(e){
    ym(25464830,'reachGoal','faq');
  });
  $('#partner_form').submit(function(e){
    ym(25464830,'reachGoal','partner')
  });
  $('#consult, #hr_form').submit(function(e){
    ym(25464830,'reachGoal','kosultwork')
  });
  $('#kosultwork').submit(function(e){
    ym(25464830,'reachGoal','kosultwork')
  });
  $('#resume').submit(function(e){
    ym(25464830,'reachGoal','resume')
  });
  $('#cv_form').submit(function(e){
    ym(25464830,'reachGoal','resume')
  });
  $('#open_fitness').submit(function(e){
    ym(25464830,'reachGoal','otkritklub')
  });

  $(document).on('click','#chat-24-roll',function(e) {
    ym(25464830,'reachGoal','chat')
  });
  $('header .header-top .phone a, footer .phone a, .main-menu .contacts .phone, .land-section-leave a[href^="tel:"], .section-contacts a[href^="tel:"], .contacts-info .box a[href^="tel:"]').click(function (e) {
    ym(25464830,'reachGoal','Clickphone')
  });
});


