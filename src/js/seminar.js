// include Работа svg и svgsprit в старых браузерах
//= plugins/svg4everybody.min.js

// include jquery
//= ../../bower_components/jquery/dist/jquery.min.js

// include Попапы на сайте
//= ../../bower_components/fancybox/dist/jquery.fancybox.js

// include Стилизая селектов проекта
//= ../../bower_components/select2/dist/js/select2.js

// include Карусели на сайте
//= ../../bower_components/slick-carousel/slick/slick.js

// include picturefill
//= ../../bower_components/picturefill/dist/picturefill.js

// include hc-sticky
//= ../../bower_components/hc-sticky/dist/hc-sticky.js

// include Маска для input
//= plugins/mask.js

// include Пользовательские скрипты которые идут на весь сайт
//= custom/app.js

// include  Меню с якорями
//= partials/nav-anchors.js

// include  Содержание семенира
//= partials/land-contents.js

// include  Выпускные документы
//= partials/land-section-documents.js

// include  Отзывы
//= partials/section-feedback.js

// include Галлерея
//= partials/land-gallery.js
