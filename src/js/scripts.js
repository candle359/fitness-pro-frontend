// include Работа svg и svgsprit в старых браузерах
//= plugins/svg4everybody.min.js

// include jquery
//= ../../bower_components/jquery/dist/jquery.min.js

// include Попапы на сайте
//= ../../bower_components/fancybox/dist/jquery.fancybox.js

// include Стилизая селектов проекта
//= ../../bower_components/select2/dist/js/select2.js

// include Карусели на сайте
//= ../../bower_components/slick-carousel/slick/slick.js

// include picturefill
//= ../../bower_components/picturefill/dist/picturefill.js

// include hc-sticky
//= ../../bower_components/hc-sticky/dist/hc-sticky.js


// include jquery-ui.js
//= ../../bower_components/jquery-ui/jquery-ui.js

// include datepicker-ru
//= ../../bower_components/jquery-ui/ui/i18n/datepicker-ru.js

// include jquery.datepicker.extension.range.min.js
//= plugins/jquery.datepicker.extension.range.min.js

// include Подстраиваемое видео
//= partials/video-resize.js

// include Маска для input
//= plugins/mask.js

// include Анонс курсов
//= partials/courses-preview.js

// include Библиотека
//= partials/library-container.js

// include Популярные вопросы
//= partials/popular-question.js

// include Анонс курсов
//= partials/programs-container.js

// include Пользовательские скрипты которые идут на весь сайт
//= custom/app.js

// include  Меню с якорями
//= partials/nav-anchors.js

// include  filters
//= partials/filters.js

// include  Расписание список
//= partials/schedule-list.js

// include Детальная страница
//= partials/blog-detail.js

// include  Связь с редакцией
//= partials/section-callbacks.js

// include  Другие истории
//= partials/history-outer.js

// include  Отзывы
//= partials/section-feedback.js

// include  Лицензии
//= partials/section-licenses.js

// include  Учебники под редакцией
//= partials/section-book.js

// include Оплата
//= partials/payment.js

// include фотогалерея каруслеь
//= partials/gallery-carousel.js

// include Вопрос ответ
//= partials/faq-accordion.js

// include Результаты поиска
//= partials/search-result.js

// include отзывы о нас
//= partials/reviews-aboutus.js

// include отзывы о нас
//= partials/article.js

