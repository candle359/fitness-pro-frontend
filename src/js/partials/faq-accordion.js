$(document).ready(function () {
    $('.faq-accordion .head ').click(function (e) {
        e.preventDefault();
        var $parent = $(this).closest('.item');
        $parent.find('.body').stop(false, true).slideToggle(function (e) {
            $parent.toggleClass('open');
            if (!$parent.hasClass('open')) {
                $parent.find('.itm').removeClass('open')
                $parent.find('.answer').hide();
            }
        });
        // $parent.siblings().find('.body').stop().slideUp(function (e) {
        //     $(this).closest('.item').removeClass('open').find('.itm').removeClass('open')
        //     $(this).closest('.item').find('.answer').hide();
        // });
    });
    $('.faq-accordion .itm .ttl').click(function (e) {
        e.preventDefault();
        var $parent = $(this).closest('.itm');
        $parent.find('.answer').stop(false, true).slideToggle(function (e) {
            $parent.toggleClass('open');
        });
        // $parent.siblings().find('.answer').stop().slideUp(function (e) {
        //     $(this).closest('.itm').removeClass('open')
        // });
    });
});
