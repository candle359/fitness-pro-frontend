$(document).ready(function () {
    $('.histroy-outher .carousel-wrap').each(function () {
        var $prev = $(this).find('.slick-prev');
        var $next = $(this).find('.slick-next');
        $(this).find('.carousel').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            prevArrow: $prev,
            nextArrow: $next,
            dots: true,
            responsive: [
                {
                    breakpoint: 990,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        dots: true,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        dots: true,
                    }
                },

            ]
        })

    });
});
