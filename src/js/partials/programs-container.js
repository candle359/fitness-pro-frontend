$(document).ready(function () {
    tabsCarousel();
    $('.programs-container .tabs-head .item  a').click(function () {
        if ($(window).width() < 768 && !$(this).closest('.tabs-head').hasClass('not-carousel') ) {
            $(this).closest('.tabs-head').slick('slickGoTo', $(this).closest('.slick-slide').attr('data-slick-index') )
        }
    });
});


var clrProgramCrTimeOut;


$(window).resize(function () {
    clearTimeout(clrProgramCrTimeOut);
    clrProgramCrTimeOut = setTimeout(function () {
        tabsCarousel();
    }, 200);

});

function tabsCarousel() {
    $('.programs-container').each(function () {
        var $tabsHead = $(this).find('.tabs-head');
        var $prev = $(this).find('.arrow-prev');
        var $next = $(this).find('.arrow-next');
        var slideIndex =  $(this).find('.tabs-head:not(.slick-initialized) .item').length -1;
        if ($(window).width() < 768 && !$tabsHead.hasClass('not-carousel')) {
            $(this).find('.tabs-head:not(.slick-initialized)').slick({
                slidesToShow: 1,
                prevArrow: $prev,
                nextArrow: $next,
                infinite: false,
                variableWidth: true,
                dots: true
            })
                .on('afterChange', function(event, slick, currentSlide){
                    $tabsHead.find('.active').removeClass('active');
                    $tabsHead.find('[data-slick-index="'+currentSlide+'"] a').trigger('click')
                });
            if (slideIndex &&  $(this).find('.tabs-head .item-all').length) {
                $tabsHead.slick('slickRemove',slideIndex);
            }

        } else {
            $(this).find('.slick-initialized').slick('unslick');
        }
    });

}
