$(document).ready(function () {
    $('.library-container .carousel-wrap').each(function () {
        var $prev = $(this).find('.slick-prev');
        var $next = $(this).find('.slick-next');
        var $prevM = $(this).find('.arrow-prev');
        var $nextM = $(this).find('.arrow-next');
        $(this).find('.carousel').slick({
            slidesToShow: 1,
            prevArrow: $prev,
            nextArrow: $next,
            infinite: true,
            lazyLoad: 'ondemand',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        lazyLoad: 'progressive',
                        dots: true,
                        prevArrow: $prevM,
                        nextArrow: $nextM,
                        infinite: false,
                        variableWidth: true
                    }
                },

            ]
        })
            .on('lazyLoaded', function(event, slick, image, imageSource){
                picturefill({
                    reevaluate: true
                });
            });

    });
});
