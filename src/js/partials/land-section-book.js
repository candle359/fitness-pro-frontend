$(document).ready(function () {
    $(".textbook-pdf").each(function () {
        var pdf = $(this).data('pathpdf');
        $(this).flipBook({
            pdfUrl: pdf,
            sound: false,
            btnShare : {enabled:false},
            btnSound:  {enabled:false},
            btnPrint : {
                hideOnMobile:true
            },
        });
    })
});

