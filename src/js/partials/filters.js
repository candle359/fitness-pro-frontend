$(document).ready(function () {
    $('.filter-mob-btn .btn-filter').click(function (e) {
        e.preventDefault();
        $($(this).attr('href')).addClass('open');
    });
    $('.filter .close, .filter button[type="reset"]').click(function (e) {
        e.preventDefault();
        $(this).closest('.filter-wrap').removeClass('open');
    });
});
