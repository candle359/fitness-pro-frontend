$(document).ready(function () {
    $('.gallery-carousel').each(function () {
        var $prev = $(this).find('.slick-prev');
        var $next = $(this).find('.slick-next');

        $(this).find('.carousel').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: $prev,
            nextArrow: $next,
            responsive: [
                {
                    breakpoint: 992,

                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        dots: true,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true,
                    }
                },

            ]
        })

    });
});
