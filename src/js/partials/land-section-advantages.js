$(document).ready(function () {
    $('.land-section-advantages .js-more').click(function (e) {
        e.preventDefault();
        var txt = ($(this).closest('.land-section-advantages').find('.hidden-phone').is(':visible'))? 'Показать еще' : 'Скрыть';
        $(this).text(txt).closest('.land-section-advantages').find('.hidden-phone').stop().slideToggle();
    });
});
