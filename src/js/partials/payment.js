$(document).on('click', '.js-copy', function (e) {
        var target = e.currentTarget;
        var input = document.createElement('textarea');
        document.body.append(input);
        var arr = target.getAttribute('data-text').split('\n');
        arr.forEach(function(item) {
            input.value = input.value + item.trim() + '\n';
        });
        input.select();
        input.setSelectionRange(0, 99999);
        document.execCommand('copy');
        document.body.removeChild(input);
    });

