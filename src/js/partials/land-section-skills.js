$(document).ready(function () {
    $('.land-section-skills .js-more').click(function (e) {
        e.preventDefault();
        var txt = ($(this).closest('.land-section-skills').find('.hidden-phone').is(':visible'))? 'Показать еще' : 'Скрыть';
        $(this).text(txt).closest('.land-section-skills').find('.hidden-phone').stop().slideToggle();
    });
});
