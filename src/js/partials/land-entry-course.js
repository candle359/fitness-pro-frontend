$(document).ready(function () {
  if ($('.land-entry-course.sticky-block').length > 0) {
    var top = $('header .header-bottom').outerHeight() + $('.nav-anchors').outerHeight() +15;
    $('.land-entry-course.sticky-block').each(function (e) {
      $(this).hcSticky({
        stickTo: $(this).data('parent'),
        top: top,
        responsive: {
          992: {
            disable: true
          }
        }
      });
    });
  }
});


