$(document).ready(function () {
    $('.land-section-feedback').each(function () {
        var $prev = $(this).find('.slick-prev');
        var $next = $(this).find('.slick-next');
        $(this).find('.carousel:not(.slick-initialized)').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            prevArrow: $prev,
            nextArrow: $next,
            dots: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        })
    })
});
