$(document).ready(function () {
    infoCarousel();
    $('.land-section-info .js-carousel-more').click(function (e) {
        e.preventDefault();
        var txt = ($(this).closest('.item').find('.hidden-phone').is(':visible'))? 'Показать еще' : 'Скрыть';
        $(this).toggleClass('active').find('span').text(txt).closest('.item').find('.hidden-phone').stop().slideToggle();
    });
    $('.land-section-info   .js-more').click(function (e) {
        e.preventDefault();
        var txt = ($(this).closest('.land-section-info ').find('.hidden-phone').is(':visible'))? 'Показать еще' : 'Скрыть';
        $(this).text(txt).closest('.land-section-info').find('.hidden-phone').stop().slideToggle();
    });
});
var clrInfoCrTimeOut;


$(window).resize(function () {
    clearTimeout(clrInfoCrTimeOut);
    clrInfoCrTimeOut = setTimeout(function () {
        infoCarousel();
    }, 200);


});
function infoCarousel() {
    $('.land-section-info').each(function () {
        var $prev = $(this).find('.arrow-prev');
        var $next = $(this).find('.arrow-next');
        if ($(window).width() < 768) {
            $(this).find('.carousel:not(.slick-initialized)').slick({
                prevArrow: $prev,
                nextArrow: $next,
                infinite: false,
                dots: true
            })
                .on('beforeChange', function(event, slick, currentSlide, nextSlide){
                    $(this).find('[data-slick-index="'+currentSlide+'"] .js-carousel-more.active').trigger('click');
                });
        } else {
            $(this).find('.slick-initialized').slick('unslick');
        }
    })
}
