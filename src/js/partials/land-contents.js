$(document).ready(function () {
    $('.land-contents  .js-more').click(function (e) {
        e.preventDefault();
        var txt = ($(this).closest('.land-contents').find('.hidden-phone').is(':visible')) ? 'Показать еще' : 'Скрыть';
        $(this).text(txt).closest('.land-contents').find('.hidden-phone').stop().slideToggle();
    });
});
