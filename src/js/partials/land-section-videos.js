$(document).ready(function () {
    $('.land-section-videos .js-more').click(function (e) {
        e.preventDefault();
        var txt = ($(this).closest('.description').find('.hidden-phone').is(':visible'))? 'Показать еще' : 'Скрыть';
        $(this).text(txt).closest('.description').find('.hidden-phone').stop().slideToggle();
    });
    $('.land-section-videos').each(function () {
        var $prev = $(this).find('.slick-prev');
        var $next = $(this).find('.slick-next');
        $(this).find('.carousel:not(.slick-initialized)').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            prevArrow: $prev,
            nextArrow: $next,
            lazyLoad: 'ondemand',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        dots: true,
                        arrows: false,
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        dots: true,
                        arrows: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        dots: true,
                        arrows: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        })
    })
});
