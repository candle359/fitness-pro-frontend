$(document).ready(function () {
    $('.land-section-desc .js-more').click(function (e) {
        e.preventDefault();
        var txt = ($(this).hasClass('active'))? 'Показать еще' : 'Скрыть';
        $(this).toggleClass('active').text(txt).closest('.land-section-desc').find('.description').toggleClass('open')
    });
});
