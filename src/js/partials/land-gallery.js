$(document).ready(function () {
    $('.land-section-gallery').each(function () {
        var $prev = $(this).find('.slick-prev');
        var $next = $(this).find('.slick-next');
        $(this).find('.carousel:not(.slick-initialized)').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: $prev,
            nextArrow: $next,
            responsive: [
                {
                    breakpoint: 992,
                    dots: true,
                    arrows: false,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    dots: true,
                    arrows: false,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        dots: true,
                        arrows: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        })
    })
});
