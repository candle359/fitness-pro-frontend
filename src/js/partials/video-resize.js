$.fn.videoResize = function (options) {
    var $obj = this;
    var resize = function () {
        $obj.each(function () {
            var $video = $(this),
                width = $video.closest($video.data('parent')).width(),
                height = $video.closest($video.data('parent')).height(),
                ratio = $video.data('ratio').split('/')[0] / $video.data('ratio').split('/')[1],
                playerWidth,
                playerHeight;
            if (width / ratio < height) {
                playerWidth = Math.ceil(height * ratio);
                $video.width(playerWidth).height(height);
            } else {
                playerHeight = Math.ceil(width / ratio);
                $video.width(width).height(playerHeight);
            }
        });
    };
    resize();
    var clrTimeOut;
    $(window).on('resize', function () {
        clearTimeout(clrTimeOut);
        clrTimeOut = setTimeout(function () {
            resize();
        }, 200);
    });
};
