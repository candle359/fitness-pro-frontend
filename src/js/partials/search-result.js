$(document).ready(function () {
    $('.search-filter .head a').click(function (e) {
       e.preventDefault();
        $(this).closest('.search-filter').toggleClass('open');
        $(this).closest('.search-filter').find('ul').stop().slideToggle();
    });
    $('.search-filter ul a').click(function (e) {
       e.preventDefault();
       var txt = $(this).text();
        $(this).closest('li').addClass('active').siblings().removeClass('active');
        $(this).closest('.search-filter').find('.head a span').text(txt);
        $(this).closest('.search-filter').removeClass('open').find('ul').stop().slideUp();

        $('html, body').stop().animate({ scrollTop: $($(this).attr('href')).offset().top - $('.header-bottom').outerHeight() }, 400);
    });
    $(document).click( function(event){
        if( $(event.target).closest('.search-filter').length)
            return;
        $('.search-filter').removeClass('open').find('ul').stop().slideUp();
        event.stopPropagation();
    });
});
