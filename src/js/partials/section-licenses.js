$(document).ready(function () {
    $('.section-licenses .carousel-wrap').each(function () {
        var $prev = $(this).find('.slick-prev');
        var $next = $(this).find('.slick-next');

        $(this).find('.carousel').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: $prev,
            nextArrow: $next,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        dots: true,
                    }
                },

            ]
        })

    });
});
