$(document).ready(function () {
    $('.land-section-documents  .carousel-wrap ').each(function () {
        var $prev = $(this).find('.arrow-prev');
        var $next = $(this).find('.arrow-next');
        var slideToShow = ($(this).find('.item').length > 2)? 3: ($(this).find('.item').length > 2) ? 1: 2;
        $(this).find('.carousel').slick({
            arrows: false,
            prevArrow: $prev,
            nextArrow: $next,
            slidesToShow: slideToShow,
            speed: 300,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        speed: 150,
                        arrows: true,
                        dots: true,
                        slidesToShow: 1,
                        centerPadding: '20px',
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        speed: 150,
                        slidesToShow: 1,
                        arrows: true,
                        dots: true,
                    }
                }
            ]
        });
    });
    $(document).on('beforeClose.fb', function( e, instance, slide ) {
        console.log('Перед закрытием окна');
        $('.land-section-documents .slick-initialized').each(function () {
                $(this).slick('refresh');
        });
    });
});
