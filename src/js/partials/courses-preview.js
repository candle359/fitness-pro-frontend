$(document).ready(function () {
    $('.courses-preview .carousel').each(function () {
        var prev = $(this).closest('.courses-preview').find('.slick-prev');
        var next  = $(this).closest('.courses-preview').find('.slick-next');
        $(this).slick({
            prevArrow: prev,
            nextArrow: next,
            fade: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        arrows: false,
                        dots: true
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        adaptiveHeight: true,
                        dots: true
                    }
                }
            ]
        });
    });
});
