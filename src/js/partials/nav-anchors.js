$(document).ready(function () {
    if ($('.nav-anchors').length > 0) {
        var top = $('header .header-bottom').outerHeight();
        $('.nav-anchors').each(function (e) {
            $(this).hcSticky({
                stickTo: $(this).data('parent'),
                top: top,
                responsive: true,
            });
        });
    }
    navAchore();
    $('.nav-anchors a, .anchore').bind('click', function (event) {
        $($(this).attr('href'))
        var $anchor = $(this).attr('href'),
            top = $($anchor).offset().top - 68;
        $('html, body').stop().animate({
            scrollTop: top
        }, 1200);
        event.preventDefault();
    });
});
$(window).scroll(function () {
    navAchore();
});
function navAchore() {
    var scrTop = $(window).scrollTop() + $('header .header-bottom').outerHeight() + $('.nav-anchors').outerHeight();
    $('.nav-anchors a').each(function () {
        if ($($(this).attr('href')).length) {
            var $sct = $($(this).attr('href'));
            var topSct = $sct.offset().top;
            var btmSct = topSct + $sct.outerHeight();
            if (scrTop > topSct && scrTop < btmSct) {
                $(this).closest('li').addClass('active');
            } else {
                $(this).closest('li').removeClass('active');
            }
        }
    });
}
