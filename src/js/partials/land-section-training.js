$(document).ready(function () {
    traningCarousel();
});
var clrTraningtCrTimeOut;


$(window).resize(function () {
    clearTimeout(clrTraningtCrTimeOut);
    clrTraningtCrTimeOut = setTimeout(function () {
        traningCarousel();
    }, 200);


});
function traningCarousel() {
    $('.land-section-training').each(function () {
        var $prev = $(this).find('.arrow-prev');
        var $next = $(this).find('.arrow-next');
        if ($(window).width() < 768) {
            $(this).find('.list:not(.slick-initialized)').slick({
                prevArrow: $prev,
                nextArrow: $next,
                infinite: false,
                dots: true
            })
        } else {
            $(this).find('.slick-initialized').slick('unslick');
        }
    })
}
