$(document).ready(function () {
    $('.section-books').each(function () {
        var $prev = $(this).find('.slick-prev');
        var $next = $(this).find('.slick-next');
        $(this).find('.carousel').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            prevArrow: $prev,
            nextArrow: $next,
            infinite: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true,
                        arrows: false
                    }
                },

            ]
        });


    });
});
