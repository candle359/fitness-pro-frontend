$(document).ready(function () {
    $('.popular-question .tabs-head a').click(function (e) {
        $('.tab-lnk[href="'+$(this).attr('href')+'"]').addClass('open').siblings().removeClass('open');
    });
    $('.popular-question .tab-lnk').click(function (e) {
        e.preventDefault();
        var $this = $(this);
        $('.popular-question .tabs-head a[href="'+$this.attr('href')+'"]').parent().addClass('active').siblings().removeClass('active');
        $this.toggleClass('open').siblings().removeClass('open');
        $($this.attr('href')).stop().slideToggle(function () {
            $($this.attr('href')).toggleClass('active')
        }).siblings('.tab').stop().slideUp(function () {
            $($this.attr('href')).siblings().removeClass('active');
        });
    });
});
