$(document).ready(function () {
    if ($('.blog-detail .sticky-block').length > 0) {
        var top = $('header .header-bottom').outerHeight() + 15;
        $('.blog-detail .sticky-block').each(function (e) {
            $(this).hcSticky({
                stickTo: $(this).data('parent'),
                top: top,
                responsive: {
                    767: {
                        disable: true
                    }
                }
            });
        });
    }
    $('.blog-detail .sources .btn').click(function (e) {
        e.preventDefault();
        var txt = (!$(this).closest('.sources').find('.list').hasClass('open')) ? 'Скрыть':'Показать еще';
        $(this).text(txt).closest('.sources').find('.list').toggleClass('open');
    });
});


