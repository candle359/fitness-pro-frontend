$(document).ready(function(){
  $('.reviews-aboutus').each(function () {
    var $prev = $(this).find('.slick-prev');
    var $next = $(this).find('.slick-next');
    $(this).find('.top .carousel').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      prevArrow: $prev,
      nextArrow: $next,
      focusOnSelect: true,
      asNavFor: $(this).find('.btm .carousel'),
      responsive: [
        {
          breakpoint: 992,
          settings: {
            dots: true,
            arrows: false,
            slidesToShow: 3,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 768,
          settings: {
            dots: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ]
    });
    $(this).find('.btm .carousel').slick({
      fade: true,
      asNavFor: $(this).find('.top .carousel'),
      arrows: false,
    });
  })
});