$(document).ready(function () {
    workCarousel();
});
var clrWorkCrTimeOut;
$(window).resize(function () {
    clearTimeout(clrWorkCrTimeOut);
    clrWorkCrTimeOut = setTimeout(function () {
        workCarousel();
    }, 200);
});
function workCarousel() {
    $('.land-section-work').each(function () {
        var $prev = $(this).find('.arrow-prev');
        var $next = $(this).find('.arrow-next');
        if ($(window).width() < 768) {
            $(this).find('.list:not(.slick-initialized)').slick({
                prevArrow: $prev,
                nextArrow: $next,
                infinite: false,
                dots: true
            })
        } else {
            $(this).find('.slick-initialized').slick('unslick');
        }
    })
}
