$(document).ready(function () {
    if ($('.countdown').length) {
        $('.countdown').each(function () {
            $(this).countdown();
        });
    }
});
$.fn.countdown = function () {
    var timer, days, hours, minutes, seconds;
    var $this = $(this);
    var dateEnd = new Date($this.data('dateend'));
    dateEnd = dateEnd.getTime();
    if (isNaN(dateEnd)) {
        return;
    }

    timer = setInterval(calculate, 1000);

    function calculate() {
        var dateStart = new Date();
        var dateStart = new Date(dateStart.getFullYear(),
            dateStart.getMonth(),
            dateStart.getDate(),
            dateStart.getHours(),
            dateStart.getMinutes(),
            dateStart.getSeconds());
        var timeRemaining = parseInt((dateEnd - dateStart.getTime()) / 1000)
        if (timeRemaining >= 0) {
            days = parseInt(timeRemaining / 86400);
            timeRemaining = (timeRemaining % 86400);
            hours = parseInt(timeRemaining / 3600);
            timeRemaining = (timeRemaining % 3600);
            minutes = parseInt(timeRemaining / 60);
            timeRemaining = (timeRemaining % 60);
            seconds = parseInt(timeRemaining);
            $this.find('.days').html(parseInt(days, 10));
            $this.find('.hours').html(("0" + hours).slice(-2));
            $this.find('.minutes').html(("0" + minutes).slice(-2));
            // $this.find('.seconds').html(("0" + seconds).slice(-2));
        } else {
            return;
        }
    }

    function display(days, hours, minutes, seconds) {
    }
}
