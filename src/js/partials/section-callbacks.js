$(document).ready(function () {
    $('.section-collbacks .lnk-abs').click(function (e) {
        var theme = $(this).data('theme');
        $($(this).attr('href')).removeClass('success');
        $($(this).attr('href')).find('.title').text(theme);
        $($(this).attr('href')).find('input[name="theme"]').val(theme);
    });
});
