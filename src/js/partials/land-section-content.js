$(document).ready(function () {
    countentCarousel();
    $(document).on('click','.land-section-content .js-carousel-more',function(e) {
        e.preventDefault();
        var txt = ($(this).closest('.item').find('.hidden-phone').is(':visible'))? 'Показать еще' : 'Скрыть';
        $(this).toggleClass('active').find('span').text(txt).closest('.item').find('.hidden-phone').stop().slideToggle();
    });
    $('.land-section-content   .js-more').click(function (e) {
        e.preventDefault();
        var txt = ($(this).closest('.land-section-content ').find('.hidden-phone').is(':visible'))? 'Показать еще' : 'Скрыть';
        $(this).text(txt).closest('.land-section-content').find('.hidden-phone').stop().slideToggle();
    });
});
var clrContentCrTimeOut;


$(window).resize(function () {
    clearTimeout(clrContentCrTimeOut);
    clrContentCrTimeOut = setTimeout(function () {
        countentCarousel();
    }, 200);


});
function countentCarousel() {
    $('.land-section-content').each(function () {
        var $prev = $(this).find('.arrow-prev');
        var $next = $(this).find('.arrow-next');
        if ($(window).width() < 768) {
            $(this).find('.carousel:not(.slick-initialized)').slick({
                prevArrow: $prev,
                nextArrow: $next,
                infinite: false,
                dots: true
            })
                .on('beforeChange', function(event, slick, currentSlide, nextSlide){
                    $(this).find('[data-slick-index="'+currentSlide+'"] .js-carousel-more.active').trigger('click');
                });
        } else {
            $(this).find('.slick-initialized').slick('unslick');
        }
    })
}
