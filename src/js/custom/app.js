var uikit = {

    isDesktop: function () {
        return ($(window).width() > 991) ? true : false;
    },
    isTabletWide: function () {
        return ($(window).width() > 767 && $(window).width() < 992) ? true : false;
    },
    isTablet: function () {
        return ($(window).width() < 768 && $(window).width() > 479) ? true : false;
    },

    isPhoneWide: function () {
        return ($(window).width() < 480) ? true : false;
    },

    isTouch: function () {
        try {
            document.createEvent("TouchEvent");
            return true;
        } catch (e) {
            return false;
        }
    },

    svg: function () {
        svg4everybody({});
    },

    select: function () {
        var icnAngle = '<svg class="icon icon_angle-down"><use xlink:href="images/sprite-svg.svg#angle-down"></use></svg>';
        $('.js-select').each(function () {
            var pl = $(this).attr('placeholder'),
                search = ($(this).hasClass('is-search')) ? '' : Infinity,
                type = '',
                typeSelSize = '';
            if ($(this).hasClass('input-xs')) {
                typeSelSize = 'select2-dropdown_select_xs';
            }
            if ($(this).hasClass('border-white')) {
                type = 'border-white';
            }

            $(this).select2({
                language: 'ru',
                placeholder: pl,
                minimumResultsForSearch: search
            })
                .on('select2:open', function (e) {
                    $('.select2-dropdown').addClass(typeSelSize);
                    $('.select2-dropdown').addClass(type);
                });
        });
        $('.select2-selection__arrow b').append(icnAngle);

    },

    validation: function () {
        $('.form-control').focus(function () {
            $(this).removeClass('error').closest('.form-group').removeClass('error');
        });
        $('select').change(function () {
            $(this).removeClass('error').closest('.form-group').removeClass('error');
        });
        $('input[type="checkbox"]').change(function () {
            $(this).removeClass('error').closest('.form-group').removeClass('error');
        });
        $('[name="phone"]').bind("keydown", function(e) {
            var keyKode = e.keyCode;
            if (keyKode>95 && keyKode<106 || keyKode>47 && keyKode<58 || keyKode == 8 || keyKode == 46 || keyKode ==9 || keyKode ==189  || keyKode ==187 || keyKode ==107 || keyKode ==32) { } else return false;
        });
        $('form').each(function () {
            var item = $(this),
                btn = item.find('.js-validate'),
                btnConsult = item.find('.consult-request');
            function checkInput() {
                item.find('select.required').each(function () {
                    if ($(this).val() != '') {
                        $(this).removeClass('error').closest('.form-group').removeClass('error');
                    } else {
                        $(this).addClass('error').closest('.form-group').addClass('error').find('.error-message').show();
                    }
                });
                item.find('input[type=text].required, input[type=phone].required').each(function () {
                    if ($(this).val() != '') {
                        $(this).removeClass('error').closest('.form-group').removeClass('error');
                    } else {
                        $(this).addClass('error').closest('.form-group').addClass('error');
                    }
                });
                item.find('input[type=password].required').each(function () {
                    if ($(this).val() != '') {
                        $(this).removeClass('error').closest('.form-group').removeClass('error');
                    } else {
                        $(this).addClass('error').closest('.form-group').addClass('error');

                    }
                });
                item.find('[name="phone"]').each(function () {
                    var regExp = /(\+\d{1,3}|\d{1,3})[- _]*\(?[- _]*(\d{3}[- _]*\)?([- _]*\d){7}|\d\d[- _]*\d\d[- _]*\)?([- _]*\d){6,9})/g;
                    var $this = $(this);
                    if ($this.val().length > 10) {
                        $this.removeClass('error').closest('.form-group').removeClass('error');
                    } else {
                        $this.addClass('error').closest('.form-group').addClass('error');
                    }
                });
                if ($('.pass1', item).length != 0) {
                    var pass01 = item.find('.pass1').val();
                    var pass02 = item.find('.pass2').val();
                    if (pass01 != pass02) {
                        $('.pass1, .pass2', item).addClass('error').closest('.form-group').addClass('error');
                    }
                }
                item.find('textarea.required').each(function () {
                    if ($(this).val() != '') {
                        $(this).removeClass('error').closest('.form-group').removeClass('error');
                    } else {
                        $(this).addClass('error').closest('.form-group').addClass('error');
                    }
                });
                item.find('input[type=email]').each(function () {
                    var regexp = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/i;
                    var $this = $(this);
                    if ($this.hasClass('required')) {
                        if (regexp.test($this.val())) {
                            $this.removeClass('error').closest('.form-group').removeClass('error');
                        } else {
                            $this.addClass('error').closest('.form-group').addClass('error');
                        }
                    } else {
                        if ($this.val() != '') {
                            if (regexp.test($this.val())) {
                                $this.removeClass('error').closest('.form-group').removeClass('error');
                            } else {
                                $this.addClass('error').closest('.form-group').addClass('error');
                            }
                        } else {
                            $this.removeClass('error').closest('.form-group').removeClass('error');
                        }
                    }
                });

                item.find('input[type=checkbox].required').each(function () {
                    if ($(this).is(':checked')) {
                        $(this).removeClass('error').closest('.form-group').removeClass('error');
                    } else {
                        $(this).addClass('error').closest('.form-group').addClass('error');
                    }
                });
            }

            btn.click(function (e) {
                e.preventDefault();
                checkInput();
                var sizeEmpty = item.find('.error:visible').size();
                if (sizeEmpty > 0 || $(this).hasClass('btn-loaded')) {
                    return false;
                } else {
                    $(this).addClass('btn-loaded').prepend('<div class="sk-fading-circle"><div class="sk-circle sk-circle-1"></div><div class="sk-circle sk-circle-2"></div><div class="sk-circle sk-circle-3"></div><div class="sk-circle sk-circle-4"></div><div class="sk-circle sk-circle-5"></div><div class="sk-circle sk-circle-6"></div><div class="sk-circle sk-circle-7"></div><div class="sk-circle sk-circle-8"></div><div class="sk-circle sk-circle-9"></div><div class="sk-circle sk-circle-10"></div><div class="sk-circle sk-circle-11"></div><div class="sk-circle sk-circle-12"></div></div>');
                    setTimeout(function () {
                        btn.removeClass('btn-loaded').find('.sk-fading-circle').remove();
                    }, 5000);
                   item.submit();
                    if (!$(this).hasClass('no-metrics')) {
                        ym(25464830, 'reachGoal', 'FormSubmit')
                    }
                }
            });

            btnConsult.click(function (e) {
                e.preventDefault();
                checkInput();
                var sizeEmpty = item.find('.error:visible').size();
                if (sizeEmpty > 0 || $(this).hasClass('btn-loaded')) {
                    return false;
                } else {
                    $(this).closest('form').addClass('consult-form');
                    $(this).addClass('btn-loaded').prepend('<div class="sk-fading-circle"><div class="sk-circle sk-circle-1"></div><div class="sk-circle sk-circle-2"></div><div class="sk-circle sk-circle-3"></div><div class="sk-circle sk-circle-4"></div><div class="sk-circle sk-circle-5"></div><div class="sk-circle sk-circle-6"></div><div class="sk-circle sk-circle-7"></div><div class="sk-circle sk-circle-8"></div><div class="sk-circle sk-circle-9"></div><div class="sk-circle sk-circle-10"></div><div class="sk-circle sk-circle-11"></div><div class="sk-circle sk-circle-12"></div></div>');
                    setTimeout(function () {
                        btnConsult.removeClass('btn-loaded').find('.sk-fading-circle').remove();
                    }, 5000);

                    var consult = item.find("input[name='type']");
                    if (consult) {
                        consult.val('consult');
                    }
                    item.submit();
                    if (!$(this).hasClass('no-metrics')) {
                        ym(25464830, 'reachGoal', 'FormSubmit')
                    }
                }
            });
        });
    },

    menu: function() {
        $('.main-menu>ul>li>a').click(function (e) {
            e.preventDefault();
            $(this).closest('li').toggleClass('open').siblings().removeClass('open');
            $(this).closest('.main-menu').addClass('submenu-open');
        });
        $(document).click( function(event){
            if( $(event.target).closest('.main-menu').length )
                return;
            $('.main-menu .open').removeClass('open')
            event.stopPropagation();
        });
        $('.burger').click(function (e) {
            e.preventDefault();
            $('.main-menu').removeClass('submenu-open');
            if ($('header').hasClass('search-open')) {
                $('header').removeClass('search-open');
                $('header .search').removeClass('open');

            } else {
                $('header').toggleClass('menu-open');
                $('html').toggleClass('menu-open');
            }
        });
        $('.main-menu .head').click(function (e) {
            e.preventDefault();
            $(this).closest('li').removeClass('open');
            $(this).closest('.main-menu').removeClass('submenu-open');
        });
    },

    headerScroll: function() {
        if ($(window).scrollTop()> $('.header-bottom').offset().top) {
            $('header').addClass('fixed');
        } else {
            $('header').removeClass('fixed');
        }
    },

    headerSearch: function() {
        $('header .search button').click(function (e) {
            var $search = $(this).closest('form');
            if (!$search.hasClass('open')) {
                e.preventDefault();
                $search.addClass('open');
                $('header').addClass('search-open');
            } else if ($search.find('input').val() == '') {
                e.preventDefault();
                $search.removeClass('open');
                $('header').removeClass('search-open');
            }
        });
        $(document).click( function(event){
            if( $(event.target).closest('header .search ').length )
                return;
            $('header .search').removeClass('open');
            $('header').removeClass('search-open');
            event.stopPropagation();
        });
    },

    footerEmpty: function() {
        if ($('.footer-empty').length) {
            $('.footer-empty').height($('footer').outerHeight());

        }
    },

    mask: function() {

        $('[data-mask]').each(function () {
            if (!$(this).data('mask') == '+7 (999) 999-99-99') {
                $(this).mask($(this).data('mask'))
            }

        })
    },

    tabs: function() {
      $(document).on('click','.tabs-head a',function(e) {
          if ($($(this).attr('href')).length) {
            e.preventDefault();
            $(this).parent().addClass('active').siblings().removeClass('active');
            $($(this).attr('href')).addClass('active').siblings().removeClass('active');
          }
      })
    },

    rangeDatepicker: function() {
        $('.datepicker-min').each(function() {
            var form = $(this).closest('form');
            var startDate = $(this);
            var endDate =  form.find('.datepicker-max');
            form.find('.datepicker-max, .datepicker-min').datepicker({
                range: 'period',
                onSelect: function(dateText, inst, extensionRange) {
                    $(startDate).val(extensionRange.startDateText);
                    $(endDate).val(extensionRange.endDateText);
                    if (extensionRange.startDateText != extensionRange.endDateText) {
                        $('.datepicker-max, .datepicker-min').datepicker( "hide" );
                    }
                }
            });
            form.find('.datepicker-max, .datepicker-min').datepicker('setDate');
            var extensionRange = form.find('.datepicker-max, .datepicker-min').datepicker('widget').data('datepickerExtensionRange');
        })
    },

    anchores: function() {
        if (location.hash.length) {
            var hash = '#'+location.hash.match(/#(\w+)/)[1];
            console.log(hash)
            if ($(hash).length) {
                var top = ($('.nav-anchors').length && $('.nav-anchors').is(':visible')) ? $(hash).offset().top - ($('.header-line').outerHeight() +$('.nav-anchors').outerHeight()): $(hash).offset().top - $('.header-line').outerHeight();
                console.log(top);
                $('html, body').stop().animate({
                    scrollTop: top
                }, 1200);
            }
        }
    },

    mainInit: function () {
        this.isDesktop();
        this.isTabletWide();
        this.isTablet();
        this.isPhoneWide();
        this.isTouch();
        this.svg();
        this.select();
        this.validation();
        this.headerSearch();
        this.menu();
        this.headerScroll();
        this.footerEmpty();
        this.tabs();
        this.mask();
        this.rangeDatepicker();
        this.anchores();
    }
};
$(document).ready(function () {
    uikit.mainInit();
});
var clrTimeOut;
$(window).load(function () {
    clearTimeout(clrTimeOut);
    clrTimeOut = setTimeout(function () {
        uikit.footerEmpty();
        uikit.anchores();
    }, 200);
});

$(window).resize(function () {
    clearTimeout(clrTimeOut);
    clrTimeOut = setTimeout(function () {
        uikit.isDesktop();
        uikit.isTabletWide();
        uikit.isTablet();
        uikit.isPhoneWide();
        uikit.footerEmpty();
    }, 200);

});

$(window).scroll(function () {
    uikit.headerScroll();
});
