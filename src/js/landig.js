// include Работа svg и svgsprit в старых браузерах
//= plugins/svg4everybody.min.js

// include jquery
//= ../../bower_components/jquery/dist/jquery.min.js

// include Попапы на сайте
//= ../../bower_components/fancybox/dist/jquery.fancybox.js

// include Стилизая селектов проекта
//= ../../bower_components/select2/dist/js/select2.js

// include Карусели на сайте
//= ../../bower_components/slick-carousel/slick/slick.js

// include picturefill
//= ../../bower_components/picturefill/dist/picturefill.js

// include hc-sticky
//= ../../bower_components/hc-sticky/dist/hc-sticky.js

// include Маска для input
//= plugins/mask.js

// include Пользовательские скрипты которые идут на весь сайт
//= custom/app.js

// include Счетчик обратного отсчета
//= partials/countdown.js

// include Преимущества
//= partials/land-section-advantages.js

// include  Меню с якорями
//= partials/nav-anchors.js

// include  Содержание курса
//= partials/land-section-content.js

// include  Выпускные документы
//= partials/land-section-documents.js

// include  оснащение курса
//= partials/land-section-equipment.js

// include  Информация по курсу
//= partials/land-section-info.js

// include  Отзывы
//= partials/section-feedback.js

// include  Обучение
//= partials/land-section-training.js

// include  Умение
//= partials/land-section-skills.js

// include   преподаватели
//= partials/land-section-teachers.js

// include   преподаватели
//= partials/land-section-teachers.js

// include как работает
//= partials/land-section-work.js

// include секция с описанием
//= partials/land-section-desc.js

// include секция с описанием
//= flipbook/flipbook.min.js
//= partials/land-section-book.js


// include Учебное видео
//= partials/land-section-videos.js

// include форма на ландигах
//= partials/land-entry-course.js

// include Галлерея
//= partials/land-gallery.js


