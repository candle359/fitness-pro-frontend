PUBLIC_HTML_PROD="/var/www/www-root/data/www/fitness-pro.ru"
PUBLIC_HTML_DEV="/var/www/www-root/data/www/fitness-pro.ru"
FRONTEND_FOLDER="fitness-pro-frontend"

cd $PUBLIC_HTML_DEV/$FRONTEND_FOLDER || exit
git clean -f -d
git reset --hard master
git pull origin master
npm i
gulp
rm -rf node_modules
rsync -ah -e 'sshpass -p "9G6a1U2q" ssh -oStrictHostKeyChecking=no' ./build/ admin@fitness-pro.ru:$PUBLIC_HTML_PROD/$FRONTEND_FOLDER/build
rsync -ah -e 'sshpass -p "9G6a1U2q" ssh -oStrictHostKeyChecking=no' ./src/ admin@fitness-pro.ru:$PUBLIC_HTML_PROD/$FRONTEND_FOLDER/src
#cat ./postdeploy.sh | sshpass -e ssh -oStrictHostKeyChecking=no admin@fitness-pro.ru
exit
